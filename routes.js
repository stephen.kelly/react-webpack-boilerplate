function initialize(app){

	//These are the API end points that you can write.

	//Setting up an event listener for GET request to '/'
	app.get('/', function(req, res){
		console.log('request to / received');
        res.render('index.html');
	});

    /*Routes for rendering pages in reactjs.
    After creating a page in react, define a route for it here
    */
		app.get('/index', function(req, res){
			console.log('request to /index received');
	        res.render('index.html');
		});

}
exports.initialize = initialize;

'use strict';

const ApiUrl = require('./ApiUrl');


function example(encodedData, callback) {

  console.log(encodedData);

  var xmlhttp = new XMLHttpRequest();
  // new HttpRequest instance
  xmlhttp.open("POST", ApiUrl+'dosomething', true);
  xmlhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
  // If specified, responseType must be empty string or "text"
  //xmlhttp.responseType = 'text';

  xmlhttp.onload = function () {
    console.log('Entering onload function');
    if (xmlhttp.readyState === xmlhttp.DONE) {
      console.log('readyState done');

      if (xmlhttp.status === 200) {
        console.log('status 200');
        console.log(xmlhttp.response);
        //console.log(xmlhttp.responseText);
        var result = JSON.parse(xmlhttp.response);
        callback(null, result);
      } else {
        console.log('error');
        console.log(xmlhttp.response);
        alert('Unexpected Error When doing something.');
        callback(null, 'Unexpected Error When doing something.');
      }

    } else {
      console.log('error');
      callback(null, 'Unexpected Error When doing something.');
    }
  };
  xmlhttp.send(encodedData);
}

module.exports = {
  decodeData : decodeData
};

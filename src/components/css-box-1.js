import React from 'react';
import ReactDOM from 'react-dom';
import { CSSTransitionGroup } from 'react-transition-group'; // ES6


export default class Box extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      boxContent: props.boxContent,
      box: props.box,
      toggleIcon: props.toggleIcon,
      boxSize: props.boxSize
    };
    this.removeBox = this.removeBox.bind(this);
    this.toggleCollapse = this.toggleCollapse.bind(this);
  }


  removeBox(e) {
    console.log('Remove Box Clicked');
    var css = (this.state.box === "slidedown"+this.state.boxSize+"Box") ? "slideup"+this.state.boxSize+"Box" : "slidedown"+this.state.boxSize+"Box";
    this.setState({"box":css});
  }

  toggleCollapse(e) {
    console.log('Toggle Content Clicked');
    var css = (this.state.boxContent === "slidedown"+this.state.boxSize+"Box") ? "slideup"+this.state.boxSize+"Box" : "slidedown"+this.state.boxSize+"Box";
    var icon = (this.state.boxContent  === "slidedown"+this.state.boxSize+"Box") ? "plus" : "minus";
    this.setState({
      "boxContent":css,
      "toggleIcon":icon,
    });
  }

  render() {
    var that = this,
      boxClassname, button, borderClass, loadingState, boxToolsContainer, boxTools = [];

    if(this.props.boxTools) {

      this.props.boxTools.map(function(tool, index) {

        switch(tool){
        case 'expand':
          boxClassname = "collapsed-box";
          button =
                    <button className="btn btn-box-tool" onClick={that.toggleCollapse} key={index}>
                      <i className={"fa fa-"+that.state.toggleIcon}></i>
                    </button>;

          boxTools.push(button);
          break;

        case 'collapse':
          boxClassname = '';
          button =
                    <button className="btn btn-box-tool" onClick={that.toggleCollapse} key={index}>
                      <i className={"fa fa-"+that.state.toggleIcon}></i>
                    </button>;

          boxTools.push(button);
          break;

        case 'remove':
          boxClassname = '';
          button =
                    <button className="btn btn-box-tool"  onClick={that.removeBox} key={index}>
                      <i className="fa fa-times"></i>
                    </button>;

          boxTools.push(button);
          break;
        }

      });

      boxToolsContainer = <div className="box-tools pull-right">{boxTools}</div>;
    }

    if(this.props.loading === true){
      loadingState =
            <div className="overlay">
              <i className="fa fa-refresh fa-spin"></i>
            </div>
    }

    if(this.props.border === true){
      borderClass = 'box-solid';
    }

    if(this.props.collapsed) {
      boxClassname = "collapsed-box";
    }


    return (

      <div className = {"col-md-"+this.props.width}>
        <CSSTransitionGroup
          transitionName="example"
          transitionAppear={true}
          transitionAppearTimeout={500}
          transitionEnter={false}
          transitionLeave={false}>

          <div id="Slider" className={this.state.box}>
            <div className={"box "+ this.props.theme}>
              <div className="box-header with-border">
                <h3 className="box-title">{this.props.title}</h3>
                {boxToolsContainer}
                {/* /.box-tools */}
              </div>
              <div id="Slider" className={this.state.boxContent}>
                <div className="box-body">

                  {this.props.content}
                  {this.props.children}

                </div>
                {/* /.box-body */}
              </div>
              {loadingState}
              {/* /.box */}
            </div>
          </div>

        </CSSTransitionGroup>

      </div>

    );
  }

}

Box.defaultProps = {
  boxTools: ['expand', 'remove'],
  theme: 'box-default',
  loading: false,
  border: true,
  title: 'Default title',
  content: '',
  boxContent: 'slidedownSmallBox',
  box: 'slidedownSmallBox',
  toggleIcon: 'minus',
  boxSize: 'Large'
};

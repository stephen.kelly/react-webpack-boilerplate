import React from 'react';
import ReactDOM from 'react-dom';
import { CSSTransitionGroup } from 'react-transition-group'; // ES6


export default class Callout extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      content: props.content
    };

  }

  componentWillReceiveProps(nextProps){
    var text = nextProps.content;
    console.log(text);
    this.setState({content: text});
  }


  render() {

    return (

      <div className = {"col-md-"+this.props.width}>
        <CSSTransitionGroup
          transitionName="example"
          transitionAppear={true}
          transitionAppearTimeout={500}
          transitionEnter={false}
          transitionLeave={false}>

          <div className={"callout callout-"+this.props.theme}>
            <h4>{this.props.title}</h4>
            <p id={this.props.id}>  </p>
            {this.state.content}
          </div>

        </CSSTransitionGroup>

      </div>

    );
  }

}

Callout.defaultProps = {
  theme: 'info',
  title: 'Result',
  content: '...',
  id: 'decode-result'
};

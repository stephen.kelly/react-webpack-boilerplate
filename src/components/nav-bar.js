import React from 'react';
import ReactDOM from 'react-dom';

export default class NavBar extends React.Component {
  constructor(props) {
    super(props);
    this.state = {

    };

  }

  render() {

    return (

      <header className="main-header">
        <nav className="navbar navbar-static-top">
          <div className="container">
            <div className="navbar-header">
            <div className="element">
              <a href="#!" className="navbar-brand"><b>React</b>Boilerplate</a>
            </div>
            </div>
            {/* Collect the nav links, forms, and other content for toggling */}
            <div className="collapse navbar-collapse pull-left" id="navbar-collapse">
              <ul className="nav navbar-nav">
              </ul>

            </div>
            {/* /.navbar-collapse */}
          </div>
          {/* /.container-fluid */}
        </nav>
      </header>
    );
  }

}

NavBar.defaultProps = {

};

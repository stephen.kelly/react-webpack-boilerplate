import React from 'react';

export default function InfoTile(props) {
  if(props.children) {
    return (
      <div className = {"col-md-"+props.width+" col-sm-6 col-xs-12"}>
        <div className={"info-box "+props.theme}>
          <span className="info-box-icon">
            <i className={"fa "+props.icon}></i>
          </span>

          <div className="info-box-content">
            <span className="info-box-text">{props.subject}</span>
            <span className="info-box-number">{props.stats}</span>
            {props.children}
          </div>

          {props.content}
        </div>
      </div>
    )       
  } else {
    return (
      <div className = "col-md-3 col-sm-6 col-xs-12">
        <div className="info-box">
          <span className={"info-box-icon " + props.theme}>
            <i className={"fa "+props.icon}></i>
          </span>

          <div className="info-box-content">
            <span className="info-box-text">{props.subject}</span>
            <span className="info-box-number">{props.stats}</span>
          </div>

          {props.content}
        </div>
      </div>
    );
  }                
}


InfoTile.defaultProps = {
  content: '',
  icon: 'fa-star-o',
  stats: '0',
  subject: 'Default Subject',
  theme: 'bg-aqua'      
};

import React from 'react';

export default function ProgressBar(props) {
  var style = {
    width: props.percent + '%',
    backgroundColor: props.color
  };

  return (
    <div>
      <div className="progress">
        <div className="progress-bar" style={style}></div>
      </div>
      <span className="progress-description">
        {props.description}
      </span>
    </div>
  );
}

ProgressBar.defaultPropsc = {
  percent: 50,
  description: 'Default progress 50%',
  color: 'white'
};

import React from 'react';
import Button from 'react-bootstrap/lib/Button';
import Popover from 'react-bootstrap/lib/Popover';
import Tooltip from 'react-bootstrap/lib/Tooltip';
import Modal from 'react-bootstrap/lib/Modal';
import OverlayTrigger from 'react-bootstrap/lib/OverlayTrigger';

export default class ModalExample extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      showModal: props.showModal
    };
    this.close = this.close.bind(this);
    this.open = this.open.bind(this);
  }

  close() {
    this.setState({ showModal: false });
    this.props.handleParentState();
  }

  open() {
    this.setState({ showModal: true });
  }

  componentWillReceiveProps(nextProps){
    console.log(nextProps.showModal);
    this.setState({ showModal: nextProps.showModal });

  }

  render() {

    return (
      <div>
        <Modal show={this.state.showModal} onHide={this.close}>
          <Modal.Header closeButton>
            <Modal.Title> {this.props.title} </Modal.Title>
          </Modal.Header>
          <Modal.Body>

            {this.props.children}

          </Modal.Body>
          <Modal.Footer>
            <Button onClick={this.close}>Close</Button>
          </Modal.Footer>
        </Modal>
      </div>
    );
  }
}

ModalExample.defaultProps = {
  theme: 'primary',
  size: '',
  text: 'Click Me',
  title: 'Default Title',
  //showModal: false
};

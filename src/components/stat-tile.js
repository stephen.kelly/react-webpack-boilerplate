import React from 'react';

export default function StatTile(props) {
  var link = '',
    stats = <h3> {props.stats} </h3>;

  if(props.link) {
    link =
      <a href={props.link} className="small-box-footer">
        More info <i className="fa fa-arrow-circle-right"></i>
      </a>;
  }

  if(props.stats.indexOf('%') !== -1) {
    var style = {
      fontSize: '20px'
    };

    stats =
      <h3>
        {props.stats.replace(/%/g, '')}
        <sup style={style}>%</sup>
      </h3>
  }

  return(
    <div className = {"col-lg-"+props.width+" col-xs-6"}>
      <div className={"small-box "+props.theme}>
        <div className="inner">
          {stats}
          <p>{props.subject}</p>
        </div>
        <div className="icon">
          <i className={"fa "+props.icon}></i>
        </div>
        {link}
      </div>
    </div>
  );
}

StatTile.defaultProps = {
  color: 'bg-yellow',
  icon: 'ion-person-add',
  subject: 'Default Subject',
  stats: '0',
  link: '/default/link'
};

import React from 'react';
import ReactDOM from 'react-dom';
import NavBar from '../../../components/nav-bar';
//import Callout from '../../../components/callout';
import InfoTile from '../../../components/info-tile/info-tile';
import Modal from '../../../components/modal';
import CSSBox from '../../../components/css-box-1';
import CSSBox2 from '../../../components/css-box-2';

//Css
import ThemeCSS from '../../../css/AdminLTE.min.css';
import StyleCSS from '../../../css/Style.css';
import SkinsCSS from '../../../css/skins/_all-skins.min.css';


import defaultUserImage from '../../../img/defaultUserImage.png';

class Index extends React.Component {
  constructor() {
    super();
    this.state = {

    }
  }

  render() {

    return (

      <div className="wrapper">
          <NavBar />
          {/* Full Width Column */}
          <div className="content-wrapper">
            <div className="container">
              {/* Content Header (Page header) */}
              <section className="content-header">
                <h1>
                  React
                  <small>Boilerplate</small>
                </h1>
                <ol className="breadcrumb">
                  <li><a href="#!"><i className="fa fa-wrench" /> React Boilerplate</a></li>
                  <li className="active">Index</li>
                </ol>
              </section>
              {/* Main content */}
              <section className="content">
                <div className="row">

                  

                </div>

              </section>
              {/* /.content */}
            </div>
            {/* /.container */}
          </div>
          {/* /.content-wrapper */}
          <footer className="main-footer">
            <div className="container">
              <div className="pull-right hidden-xs">
                <b>Version</b> 1.0.0
              </div>
              <strong><a>React Boilerplate</a></strong> By Stephen Kelly.
            </div>
            {/* /.container */}
          </footer>
        </div>

    )
  }
}

ReactDOM.render(
  <Index />,
  document.getElementById('index-container')
);

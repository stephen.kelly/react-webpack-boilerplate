# React Boilerplate
This is a starting point for a react project.

## Getting Started
Clone a local copy of the repository and then install the dependencies.
```
> npm install
```

## TODO

## Building
- Execute `npm run build` to build the app
- Execute `npm run dev` to serve the app on http://localhost:8000

module.exports = {
  env: {
    browser: true,
    commonjs: true,
    es6: true,
    node: true,
  },
  extends: 'eslint:recommended',
  parserOptions: {
    sourceType: 'module',
    "ecmaFeatures": {
          "jsx": true,
      }
  },
  rules: {
  //  'comma-dangle': ['error', 'always-multiline'],
    indent: ['warn', 2],
    'no-undef': ['warn'],
    'no-mixed-spaces-and-tabs': ['warn'],
    'no-constant-condition': ['warn'],
    'no-extra-semi': ['warn'],
    'no-redeclare': ['warn'],
    'no-unexpected-multiline': ['warn'],
    'no-empty': ['warn'],
    'no-cond-assign': ['warn'],
    'no-extra-boolean-cast': ['warn'],
  //  'linebreak-style': ['error', 'unix'],
  //  quotes: ['error', 'single'],
    //semi: ['error', 'always'],
    'no-unused-vars': ['warn'],
    'no-console': 0,
    'no-inner-declarations': ['warn']

  },
};
